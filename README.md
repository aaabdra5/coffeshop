# CoffeShop

**Каждую новую задачу необходимо начинать с этих действий:**
1. Локально (в visual studio) переключиться на ветку мастер
1. Стянуть последние изменения из удаленного репозитория (PULL)
1. Создать новую ветку с названием task(номер задачи)


**После выполнения задачи необходимо выполнить следующие действия:**
1. Зафиксировать изменения локально (COMMIT)
1. Опубликовать ветку с последними изменениями (PUSH)
1. В гитлабе (https://gitlab.com/aaabdra5/coffeshop) перейти во вкладку Запрос на слияние и создать новый запрос
1. После этого в выполненной задаче удалить метку Opened и добавить метку In Testing
